
const expresiones = {
    cedula: /^\d{1,10}$/,
    nombres: /^[a-zA-ZÀ-ÿ\s\ ]{5,100}$/,
    direccion: /^[a-zA-Z0-9\,\.\ \-\-]{4,200}$/,
    telefono: /^\d{1,10}$/,
    correo: /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/
}


//Funcion para registro
form.addEventListener('submit', e => {
	e.preventDefault();
	const res = checkInputs();	
	if(res==0){
			const clientes = {
				"cedula": cedula.value,
				"nombres": nombres.value,
				"direccion": direccion.value,
				"telefono": telefono.value,
                "correo": correo.value
			}		
			fetch(`${API_URL}/add`, {
				method: 'POST',
				headers: {               
				},
				body: JSON.stringify(clientes)
			})
				.then(response => response.text())
				.then(text => {
					alert(text)
					location.href = "formulario.html"
				})
		}		
});
app.post('/add', (req, res) => {
  const sql = "INSERT INTO clientes SET ?"  
  const usuarioObj = {
    cedula: req.body.cedula,
    nombres: req.body.nombres,
    direccion: req.body.direccion,
    telefono: req.body.telefono,
    correo: req.body.correo
	}
}
}
